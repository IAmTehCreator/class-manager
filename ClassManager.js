(function ( global ) {

	/**
	 * @private
	 * Takes a string and returns the same string with the first character converted to
	 * upper-case.
	 * @param {String} value The string to convert
	 * @return {String} The string with the first character uppercased
	 */
	function firstToUppercase ( value ) {
		return value.charAt(0).toUpperCase() + value.slice(1);
	}

	/**
	 * @private
	 * Creates a property getter method on the provided class prototype.
	 * @param {Object} proto The prototype of the class to add the method to
	 * @param {String} property The name of the property
	 */
	function createGetter ( proto, property ) {
		var propName = firstToUppercase(property);

		if ( !proto['get' + propName] ) {
			proto['get' + propName] = function getter () {
				return this[property];
			}
		}
	}

	/**
	 * @private
	 * Creates a property setter method on the provided class prototype.
	 * @param {Object} proto The prototype of the class to add the method to
	 * @param {String} property The name of the property
	 */
	function createSetter ( proto, property ) {
		var propName = firstToUppercase(property);

		if ( !proto['set' + propName] ) {
			proto['set' + propName] = function setter ( value ) {
				this[property] = value;
			}
		}
	}

	/**
	 * @private
	 * Itterates down the prototype chain of the provided object calling the provided function
	 * for each entity.
	 * @param {Object} proto The object whose prototype chain is to be traversed
	 * @param {Function} fn The callback function to call for each entity
	 * @param {Object} fn.entity The current entity in the prototype chain
	 */
	function traversePrototypeChain ( proto, fn ) {
		var result = false;

		while ( proto = proto.__proto__ ) {
			result = fn(proto);

			if ( result === false ) {
				break;
			}
		}
	}

	/**
	 * @private
	 * Checks if the provided method is a constructor method.
	 * @param {Function} method The method to check
	 * @return {Boolean} True if the method is a constructor, false otherwise
	 */
	function isConstructor ( method ) {
		var count = 0,
			property;

		// Constructor functions typically have properties defined on their prototype, otherwise
		// the object it would create would just be the same as {}. Since all classes created by
		// ClassManager have a callParent method then any constructor created by it will have
		// at least one defined property so this is a good marker of a constructor.
		for ( property in method.prototype ) {
			count++;
		}

		return count !== 0;
	}

	/**
	 * @private
	 * Returns the overridden implementation of the specified method from the
	 * prototype chain.
	 * @param {Object} instance The object instance to find the overridden method on
	 * @param {Function} method The method whose override is to be found
	 * @return {Function} The overridden implementation if it exists or undefined
	 */
	function getSuperMethod ( instance, method ) {
		var itterator = isConstructor(method) ? constructorItterator : methodItterator,
			superMethod = undefined,
			name = method.name,
			found = false;

		function constructorItterator ( entity ) {
			if ( entity === method.prototype ) {
				found = true;
				return;
			}

			if ( found && entity.constructor ) {
				superMethod = entity.constructor;
				return false;
			}
		}

		function methodItterator ( entity ) {
			if ( found && entity[name] ) {
				superMethod = entity[name];
				return false;
			}

			found = entity[name] === method;
		}

		traversePrototypeChain(instance, itterator);

		return superMethod;
	}

	/**
	 * @private
	 * Renames a function.
	 * @param {String} name The new name for the function
	 * @param {Function} fn The function to rename
	 * @return {Function} The renamed function
	 */
	function renameFunction ( name, fn ) {
		var impl = "return function (call) { return function " + name + " () { return call(this, arguments) }; };"
	    return (new Function(impl)())(Function.apply.bind(fn));
	};

	/**
	 * Singleton object to contain ClassManager implementation.
	 */
	global.ClassManager = {};

	/**
	 * Merges the target object into the source object.
	 * @param {Object} target The object to copy properties on to
	 * @param {Object} source The object to copy properties from
	 */
	ClassManager.merge = function merge ( target, source ) {
		var property;

		for ( property in source ) {
			if ( source.hasOwnProperty(property) ) {
				target[property] = source[property];
			}
		}
	}

	/**
	 * Defines a Javascript object constructor from the provided implementation config.
	 * @param {String} name The name of the class
	 * @param {Object} impl The object containing the class implementation
	 * @return {Function} The constructor function for the class
	 */
	ClassManager.define = function define ( name, impl ) {
		var type =  renameFunction( name, function () { this.callParent(arguments); } ),
			properties = impl.properties,
			isMixin = impl.extend === ClassManager.Mixin,
			superClass,
			propName,
			property,
			mixin,
			i;

		if ( isMixin && impl.mixins ) {
			throw 'Cannot mix mixins into a mixin, see it doesn\'t make sense!';
		}

		if ( impl.hasOwnProperty('constructor') ) {
			type = renameFunction( name, impl.constructor );

			delete impl.constructor;
		}

		if ( isMixin ) {
			type.isMixin = true;
		}

		type.PROPERTIES = properties;
		type.MIXINS = [];
		if ( impl.mixins ) {
			for ( i = 0; i < impl.mixins.length; i++ ) {
				mixin = impl.mixins[i];

				if ( !mixin.isMixin ) {
					throw 'Only mixins can be mixed into classes';
				}

				type.MIXINS.push(mixin);
				ClassManager.merge(type.prototype, mixin.prototype);
				if ( mixin.PROPERTIES ) {
					ClassManager.merge(type.PROPERTIES, mixin.PROPERTIES);
				}
			}

			delete type.prototype.isMixin;
			delete impl.mixins;
		}

		if ( !impl.destroy ) {
			type.prototype.destroy = function destroy () { this.callParent(arguments) };
		}

		if ( impl.statics ) {
			for ( property in impl.statics ) {
				type[property] = impl.statics[property];
			}

			delete impl.statics;
		}

		if ( properties ) {
			type.PROPERTIES = properties;
			for ( property in properties ) {
				createGetter(type.prototype, property);
				createSetter(type.prototype, property);
			}

			delete impl.properties;
		}

		superClass = impl.extend;
		if ( !superClass ) {
			superClass = ClassManager.Base;
		} else {
			delete impl.extend;
		}

		if ( superClass !== Object ) {
			type.SUPER = superClass;
			if ( superClass.MIXINS && superClass.MIXINS.length > 0 ) {
				for ( i = 0; i < superClass.MIXINS.length; i++ ) {
					type.MIXINS.push( superClass.MIXINS[i] );
				}
			}

			type.prototype.__proto__ = superClass.prototype;
		}

		for ( property in impl ) {
			type.prototype[property] = impl[property];
		}

		return type;
	};

	/**
	 * @class ClassManager.Mixin
	 * The base class for mixins, mixins must extend this class to be sucessfully mixed into
	 * a new class.
	 */
	ClassManager.Mixin = ClassManager.define('Mixin', {
		extend: Object,
		constructor: function () {}
	});

	/**
	 * @class ClassManager.Base
	 * A base class for other classes to extend. This base takes care of tasks like applying
	 * properties from the constructor config.
	 */
	ClassManager.Base = ClassManager.define('Base', {
		extend: Object,

		/**
		 * The base constructor, this will apply default property values and set the provided
		 * config on the instance.
		 * @param {Object} config The constructor config
		 */
		constructor: function ( config ) {
			config = config || {};

			this.initMixins(config);
			this.defaultProperties();
			this.applyProperties(config);
		},

		/**
		 * The base destructor, this will delete all registered properties from the instance.
		 */
		destroy: function () {
			this.destroyMixins();
			this.clearProperties();
		},

		/**
		 * @protected
		 * Initialises the mixins that this class uses by calling their constructor. This
		 * must be called for mixins to work correctly.
		 * @param {Object} config The configuration
		 */
		initMixins: function ( config ) {
			var mixins = this.getClass().MIXINS,
				mixin,
			 	i;

			if ( mixins.length > 0 ) {
				for ( i = 0; i < mixins.length; i++ ) {
					mixin = mixins[i];

					mixin.call(this, config);
				}
			}
		},

		/**
		 * @protected
		 * Invokes the destroy() method of all mixins giving them a chance to clean up.
		 */
		destroyMixins: function () {
			var mixins = this.getClass().MIXINS,
				mixin,
			 	i;

			if ( mixins.length > 0 ) {
				for ( i = 0; i < mixins.length; i++ ) {
					mixin = mixins[i];

					if ( mixin.prototype.hasOwnProperty('destroy') ) {
						mixin.prototype.destroy.call(this);
					}
				}
			}
		},

		/**
		 * @private
		 * Returns all of the defined properties from all of the super classes that this class
		 * has extended.
		 * @return {Object} A map of all properties
		 */
		getAllProperties: function () {
			var proto = this.getClass(),
				all = {};

			while ( proto ) {
				ClassManager.merge(all, proto.PROPERTIES);
				proto = proto.SUPER;
			}

			return all;
		},

		/**
		 * @private
		 * Itterates over all of the defined properties for this class.
		 * @param {Function} fn The callback function to be called for each property
		 * @param {String} fn.property The name of the property
		 * @param {Object} fn.value The current value of the property
		 * @param {Object} fn.initial The default value of the property
		 */
		eachProperty: function eachProperty ( fn ) {
			var me = this,
				properties = me.getAllProperties(),
				property;

			for ( property in properties ) {
				fn( property, me[property], properties[property] );
			}
		},

		/**
		 * @protected
		 * Sets all of the properties to their default values. This must be called for properties to
		 * be defaulted.
		 */
		defaultProperties: function defaultProperties () {
			var me = this;

			me.eachProperty( function ( property, value, initial ) {
				me[property] = initial || value;
			});
		},

		/**
		 * @protected
		 * Applies property values from the provided object. This must be called to copy property
		 * values from the constructor config.
		 * @param {Object} config The object to apply values from
		 */
		applyProperties: function applyProperties ( config ) {
			var me = this;

			me.eachProperty( function ( property, value, initial ) {
				me[property] = config[property] || value;
			});
		},

		/**
		 * @protected
		 * Deletes all defined properties on this instance.
		 */
		clearProperties: function clearProperties () {
			var me = this;

			me.eachProperty( function ( property, value, initial ) {
				delete me[property];
			});
		},

		/**
		 * Generic property getter. Invokes the getter method for the specified property.
		 * @param {String} name The property name to retrieve
		 * @return {Object} The property value
		 */
		get: function get ( name ) {
			return this[ 'get' + firstToUppercase(name) ]();
		},

		/**
		 * Generic property setter. Invokes the setter method for the specified property.
		 * @param {String} name The name of the property to set
		 * @param {Object} value The value to set the prope] to
		 */
		set: function set ( name, value ) {
			this[ 'set' + firstToUppercase(name) ](value);
		},

		/**
		 * Returns the constructor of this class.
		 * @return {Function} The class constructor
		 */
		getClass: function getClass () {
			return this.constructor;
		},

		/**
		 * Returns the name of the class.
		 * @return {String} The name of this class
		 */
		getClassName: function getClassName () {
			return this.getClass().name;
		},

		/**
		 * @protected
		 * Calls an overridden implementation for the currently executing method, this must
		 * be provided with the 'arguments' keyword.
		 * @param {Object} args The arguments from the calling function
		 */
		callParent: function callParent ( args ) {
			var me = this,
				superMethod;

			if ( !args || !args.callee ) {
				throw 'callParent must be called with the "arguments" keyword';
			}

			superMethod = getSuperMethod( me, isConstructor( args.callee ) ? args.callee : args.callee.caller );
			if ( superMethod ) {
				superMethod.apply( me, args );
			}
		}

	});

})( window );
