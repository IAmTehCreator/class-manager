/*
 * Copyright (c) 2015-2017 FinancialForce.com, inc. All rights reserved.
 */
/* globals Siesta */
var Harness = Siesta.Harness.Browser.ExtJS,
	expectedGlobals = [
		'ClassManager'
	];

Harness.configure({
	title: 'Class Manager Test Suite',

	preload: [
		'../ClassManager.js',
	],

	autoCheckGlobals: false,

    performSetup: false,
    maxThreads: 1,
    sandbox: false,
    sandboxCleanup: false,
});

//N.B. set 'instrument' to true to get a coverage report for a file
//Set 'instrument' to false to be able to debug through a file
Harness.start({
	group: 'Class Manager',
	autoCheckGlobals: true,
	expectedGlobals: expectedGlobals,

	items: [{
		url: 'ClassManager.t.js'
	}, {
		url: 'Base.t.js'
	}]
});
