# Javascript Class Manager
This framework allows classes to be defined in a more declarative way using configs instead of adding methods incrementally to the constructor prototype. This will be familiar to anybody that has used the ExtJS framework. The class manager supports easily extending and mixing in classes.

This class manager has been inspired by the class system in ExtJS but is much lighter weight than including the whole ExtJS framework.

## Usage
Once the framework has been loaded you can use the `ClassManager.define()` function to define classes. Below is an example `Employee` class which extends a `Person` class;

```javascript
Person = ClassManager.define('Person', {

	statics: {
		ERROR: 'Person must be given a name';
	},

	properties: {
		forename: null,
		surname: null,
		age: 0
	},

	constructor: function ( config ) {
		this.callParent(arguments);

		if ( !config.forename || !config.surname ) {
			throw Person.ERROR;
		}
	}

});

Employee = ClassManager.define('Employee', {
	extend: Person,

	properties: {
		staffId: null
	},

	constructor: function ( config ) {
		this.callParent(arguments);

		if ( !config.staffId ) {
			throw 'Member of staff must have an ID';
		}
	},

	getUniqueId: function () {
		return this.getSurname() + this.getStaffId();
	}

});

var me = new Employee({ forename: 'Steven', surname: 'Milne', staffId: 123456 }),
	id = me.getUniqueId();
```

In the example above the `Employee` class extends the `Person` class as defined by the `extend` config. All classes created with ClassManager must extend another class, if no extension class is specified then the class extends `ClassManager.Base` by default which itself extends `Object`. The ClassManager base class provides the `callParent()` method which enables a method to call it's overridden implementation further down the prototype chain. You must call the `callParent()` method in your class `constructor` and `destroy` methods to ensure parent classes get the chance to setup and cleanup.

ClassManager automatically creates getters and setters for all of the properties defined in the `properties` config, the base class also automatically sets default values for these properties and copies value into the properties in it's constructor.

ClassManager also implements mixins, these are classes which can be mixed into another class i.e. all of the methods and properties are copied onto the receiving class prototype. Mixins are a very handy way to get around the lack of multiple inheritance, using mixins multiple classes can easily share implementations. A mixin class should not be instantiated on it's own but should implement a unit of functionality which can be shared with multiple classes. Each class can only extend one other but there is no limits to how many classes can be mixed into a single class. Mixins must extend the `ClassManager.Mixin` class and can be copied into another class by being included in the `mixins` config. The mixin base class does not support `callParent()` so you should never use `callParent()` within a mixin constructor.

If the features provided by `ClassManager.Base` are too much or not enough then you can implement your own base class by extending `Object`.

## Template Methods
ClassManager supports a few template methods on the class definition which will be hooked into the lifecyle of the class. Within the class definition you can specify the following methods, if they are not specified the a default implementation will be set of `this.callParent(arguments);`.

* `constructor` - Called when the instance is being constructed, within this method the property getters and setters should be available but any property values specified in the `config` object may not be set. The constructor is provided with a config argument which should contain all of the data the constructor requires.
* `destroy` - Called when the instance is being destroyed, within this method the class should free up any resource it holds.

You should call the `callParent()` method within each of these methods to ensure parent classes get the chance to react to lifecyle events.

## API Documentation
### ClassManager.Base
A base class for other classes to extend. This base takes care of tasks like applying properties from the constructor config.

#### Methods
##### `constructor`
The base constructor, this will apply default property values and set the provided config on the instance.

##### `destroy`
The base destructor, this will delete all registered properties from the instance.

##### `get`
Generic property getter. Invokes the getter method for the specified property.

##### `set`
Generic property setter. Invokes the setter method for the specified property.

##### `getClass`
Returns the constructor of this class.

##### `getClassName`
Returns the name of the class.

##### `initMixins` (protected)
Initialises the mixins that this class uses by calling their constructor. This must be called for mixins to work correctly.

##### `destroyMixins` (protected)
Invokes the destroy() method of all mixins giving them a chance to clean up.

##### `defaultProperties` (protected)
Sets all of the properties to their default values. This must be called for properties to be defaulted.

##### `applyProperties` (protected)
Applies property values from the provided object. This must be called to copy property values from the constructor config.

##### `clearProperties` (protected)
Deletes all defined properties on this instance.

##### `callParent` (protected)
Calls an overridden implementation for the currently executing method, this must be provided with the 'arguments' keyword.

##### `getAllProperties` (private)
Returns all of the defined properties from all of the super classes that this class has extended.

##### `eachProperty` (private)
Itterates over all of the defined properties for this class.
