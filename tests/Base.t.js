describe('ClassManager.Base', function (t) {
	var TestClass,
		instance;

	t.diag('ClassManager.Base');

	t.describe('Public Methods', function (t) {
		t.beforeEach( function () {
			TestClass = ClassManager.define('TestClass', {
				extend: ClassManager.Base,
				properties: { a: 1 }
			});

			instance = new TestClass();
		});

		t.it('get(...)', function (t) {
			t.is( instance.get('a'), 1, 'Should return value from property getter');
		});

		t.it('set(...)', function (t) {
			instance.set('a', 3);
			t.is(instance.a, 3, 'Should invoke property setter with value');
		});

		t.it('getClass()', function (t) {
			t.is(instance.getClass(), TestClass, 'Should return the class constructor');
		});

		t.it('getClassName()', function (t) {
			t.is(instance.getClassName(), 'TestClass', 'Should return type name');
		});
	});

	t.describe('Protected Methods', function (t) {

		t.it('initMixins(...)', function (t) {
			var mixin1Calls = 0,
				mixin2Calls = 0,
				Mixin1 = ClassManager.define('Mixin1', {
					extend: ClassManager.Mixin,
					constructor: function () { mixin1Calls++; }
				}),
				Mixin2 = ClassManager.define('Mixin2', {
					extend: ClassManager.Mixin,
					constructor: function () { mixin2Calls++; }
				}),
				TestClass = ClassManager.define('TestClass', {
					extend: ClassManager.Base,
					mixins: [ Mixin1, Mixin2 ],

					constructor: function () {}
				}),
				instance = new TestClass();

			t.is(mixin1Calls, 0, 'SANITY - Should not call mixin 1 constructor during definition');
			t.is(mixin2Calls, 0, 'SANITY - Should not call mixin 2 constructor during definition');

			instance.initMixins();

			t.is(mixin1Calls, 1, 'Should call first mixin constructor');
			t.is(mixin2Calls, 1, 'Should call second mixin constructor');

		});

		t.it('destroyMixins()', function (t) {
			var mixin1Calls = 0,
				mixin2Calls = 0,
				Mixin1 = ClassManager.define('Mixin1', {
					extend: ClassManager.Mixin,
					destroy: function () { mixin1Calls++; }
				}),
				Mixin2 = ClassManager.define('Mixin2', {
					extend: ClassManager.Mixin,
					destroy: function () { mixin2Calls++; }
				}),
				TestClass = ClassManager.define('TestClass', {
					extend: ClassManager.Base,
					mixins: [ Mixin1, Mixin2 ]
				}),
				instance = new TestClass();

			t.is(mixin1Calls, 0, 'SANITY - Should not call mixin 1 destroy method during definition');
			t.is(mixin2Calls, 0, 'SANITY - Should not call mixin 2 destroy method during definition');

			instance.destroyMixins();

			t.is(mixin1Calls, 1, 'Should call first mixin destroy method');
			t.is(mixin2Calls, 1, 'Should call second mixin destroy method');
		});

		t.it('defaultProperties()', function (t) {
			var TestClass = ClassManager.define('TestClass', {
					extend: ClassManager.Base,
					properties: { a: 1, b: 'b', c: true },

					constructor: function () {}
				}),
				instance = new TestClass();

			t.is(instance.a, undefined, 'SANITY - Should not have defaulted property a yet');
			t.is(instance.b, undefined, 'SANITY - Should not have defaulted property a yet');
			t.is(instance.c, undefined, 'SANITY - Should not have defaulted property a yet');

			instance.defaultProperties();

			t.is(instance.a, 1, 'Should default numeric properties');
			t.is(instance.b, 'b', 'Should default string properties');
			t.is(instance.c, true, 'Should default boolean properties');
		});

		t.it('applyProperties(...)', function (t) {
			var TestClass = ClassManager.define('TestClass', {
					extend: ClassManager.Base,
					properties: { a: 1, b: 'b', c: true }
				}),
				instance = new TestClass();

			instance.applyProperties({ a: 'a', b: true, c: 2 });

			t.is(instance.a, 'a', 'Should set string properties');
			t.is(instance.b, true, 'Should set numeric properties');
			t.is(instance.c, 2, 'Should set boolean properties');
		});

		t.it('clearProperties()', function (t) {
			var TestClass = ClassManager.define('TestClass', {
					extend: ClassManager.Base,
					properties: { a: 1, b: 'b', c: true }
				}),
				instance = new TestClass();

			instance.clearProperties();

			t.expect(instance.a).not.toBeDefined();
			t.expect(instance.b).not.toBeDefined();
			t.expect(instance.c).not.toBeDefined();
		});

		t.describe('callParent(...)', function (t) {
			var superConstructorCalls = 0,
				superMethodCalls = 0,
				OtherClass = ClassManager.define('OtherClass', {
					constructor: function () {
						superConstructorCalls++;
						this.callParent(arguments);
					},
					method: function () {
						superMethodCalls++;
					}
				});

			t.beforeEach( function () {
				superConstructorCalls = 0;
				superMethodCalls = 0;
			});

			t.it('Must be passed arguments', function (t) {
				var TestClass = ClassManager.define('TestClass', {
						extend: OtherClass,

						method: function () {
							this.callParent();
						}
					}),
					instance = new TestClass();

				t.throwsOk(
					function () {
						instance.method();
					},
					'callParent must be called with the "arguments" keyword',
					'Should throw an exception'
				);
			});

			t.it('Should call super constructor', function (t) {
				var TestClass = ClassManager.define('TestClass', {
						extend: OtherClass,

						constructor: function () {
							this.callParent(arguments);
						}
					}),
					instance = new TestClass();

				t.is(superConstructorCalls, 1, 'Should call super classes constructor');
			});

			t.it('Should call super method', function (t) {
				var TestClass = ClassManager.define('TestClass', {
						extend: OtherClass,

						method: function () {
							this.callParent(arguments);
						}
					}),
					instance = new TestClass();

				t.is(superMethodCalls, 0, 'SANITY - Should not have called super method yet');
				debugger;
				instance.method();
				t.is(superMethodCalls, 1, 'Should call super classes method');
			});

			t.it('Should do nothing if no super method', function (t) {
				var TestClass = ClassManager.define('TestClass', {
						extend: OtherClass,

						thing: function () {
							this.callParent(arguments);
						}
					}),
					instance = new TestClass();

				instance.thing();
				t.is(superMethodCalls, 0, 'Should not different super class methods');
			});
		});
	});
});
