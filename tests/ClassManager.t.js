describe('Class Manager', function (t) {
	t.diag('Class Manager');

	t.describe('Definition', function (t) {
		t.it('Should return a constructor function', function (t) {
			var TestClass = ClassManager.define('TestClass', {}),
				instance = new TestClass();

			t.isFunction(TestClass, 'Should return a function');
			t.isInstanceOf(instance, TestClass, 'Constructor should return new TestClass instances');
		});

		t.it('Should create a default destroy method', function (t) {
			var TestClass = ClassManager.define('TestClass', {}),
				instance = new TestClass();

			t.isFunction(instance.destroy, 'Should create a default destroy() method');
		});

		t.it('Should use specified constructor method', function (t) {
			var calls = 0,
				TestClass = ClassManager.define('TestClass', {
					constructor: function () {
						calls++;
						this.callParent(arguments);
					}
				}),
				instance;

			t.is(calls, 0, 'Constructor should not be called during definition');
			instance = new TestClass();
			t.is(calls, 1, 'Should use specified constructor function');
		});

		t.it('Should use specified destroy method', function (t) {
			var calls = 0,
				TestClass = ClassManager.define('TestClass', {
					destroy: function () {
						calls++;
						this.callParent(arguments);
					}
				}),
				instance = new TestClass();

			t.is(calls, 0, 'Should not call the destroy method before destruction');
			instance.destroy();
			t.is(calls, 1, 'Should use specified destroy method');
		});

		t.describe('Statics', function (t) {
			t.it('Should define static properties on constructor function', function (t) {
				var TestClass = ClassManager.define('TestClass', {
						statics: {
							NUMBER: 1,
							STRING: 'test',
							BOOL: true,
							FUNCTION: function () {}
						}
					}),
					instance = new TestClass();

				t.isStrict(TestClass.NUMBER, 1, 'Should set numeric statics on class');
				t.isStrict(TestClass.STRING, 'test', 'Should set string statics on class');
				t.isStrict(TestClass.BOOL, true, 'Should set boolean statics on class');
				t.isFunction(TestClass.FUNCTION, 'Should set static functions on class');
			});

			t.it('Should not define static properties on instances', function (t) {
				var TestClass = ClassManager.define('TestClass', {
						statics: {
							NUMBER: 1,
							STRING: 'test',
							BOOL: true,
							FUNCTION: function () {}
						}
					}),
					instance = new TestClass();

				t.expect(instance.NUMBER).not.toBeDefined();
				t.expect(instance.STRING).not.toBeDefined();
				t.expect(instance.BOOL).not.toBeDefined();
				t.expect(instance.FUNCTION).not.toBeDefined();
			});
		});

		t.describe('Properties', function (t) {

			function getBasicInstance () {
				var TestClass = ClassManager.define('TestClass', {
						properties: { id: 1, title: 'Mr' }
					});

				return new TestClass();
			}

			t.it('Should default values', function (t) {
				var instance = getBasicInstance();

				t.is(instance.id, 1, 'Should default the id property');
				t.is(instance.title, 'Mr', 'Should default the title property');
			});

			t.it('Should create getter methods', function (t) {
				var instance = getBasicInstance();

				t.isFunction(instance.getId, 'Should create a getter method for the id property');
				t.is( instance.getId(), 1, 'Id getter should return value');

				t.isFunction(instance.getTitle, 'Should create a getter method for the title property');
				t.is( instance.getTitle(), 'Mr', 'Title getter should return value');
			});

			t.it('Should create setter methods', function (t) {
				var instance = getBasicInstance();

				t.isFunction(instance.setId, 'Should create setter method for the id property');
				t.isFunction(instance.setTitle, 'Should create setter method for the title property');

				instance.setId(123);
				instance.setTitle('Miss');

				t.is(instance.id, 123, 'Id setter should set id value');
				t.is(instance.title, 'Miss', 'Title setter should set title value');
			});

			t.it('Should not override existing getter and setter methods', function (t) {
				var getterCalls = 0,
					setterCalls = 0,
					TestClass = ClassManager.define('TestClass', {
						properties: { id: 1 },

						getId: function () {
							getterCalls++;
							return this.id;
						},

						setId: function ( value ) {
							setterCalls++;
							this.id = value;
						}
					}),
					instance = new TestClass();

				instance.setId(123);
				t.is( instance.getId(), 123, 'Getter should have returned the value that the setter set');

				t.is(getterCalls, 1, 'Should have called the custom getter');
				t.is(setterCalls, 1, 'Should have called the custom setter');
			});
		});

		t.describe('Mixins', function (t) {
			t.it('Should merge mixins into class', function (t) {
				var calls = 0,
					TestMixin = ClassManager.define('TestMixin', {
						extend: ClassManager.Mixin,

						mixinMethod: function () {
							calls++;
						}
					}),
					TestClass = ClassManager.define('TestClass', {
						mixins: [ TestMixin ]
					}),
					instance = new TestClass();

				t.isFunction(instance.mixinMethod, 'Should mix the mixin method into the class');

				instance.mixinMethod();
				t.is(calls, 1, 'Should be able to call mixin method from instance');
			});

			t.it('Should merge mixin properties', function (t) {
				var TestMixin = ClassManager.define('TestMixin', {
						extend: ClassManager.Mixin,
						properties: { thing: 1 }
					}),
					TestClass = ClassManager.define('TestClass', {
						mixins: [ TestMixin ],
						properties: { name: 'Bob' }
					}),
					instance = new TestClass();

				t.is(instance.thing, 1, 'Should mixin property');
				t.isFunction(instance.setThing, 'Should mixin property setter');
				t.isFunction(instance.getThing, 'Should mixin property getter');

				t.is(instance.name, 'Bob', 'Should not interfere with existing class properties');
			});

			t.it('Should not let mixins be mixed into mixins', function (t) {
				var TestMixin = ClassManager.define('TestMixin', {
						extend: ClassManager.Mixin
					});

				t.throwsOk(
					function () {
						var OtherMixin = ClassManager.define('OtherMixin', {
							extend: ClassManager.Mixin,

							mixins: [ TestMixin ]
						});
					},
					'Cannot mix mixins into a mixin, see it doesn\'t make sense!',
					'Should throw an exception'
				);
			});
		});

		t.describe('Extension', function (t) {
			t.it('Should extend ClassManager.Base by default', function (t) {
				var TestClass = ClassManager.define('TestClass', {}),
					instance = new TestClass();

				t.ok(instance instanceof ClassManager.Base, 'Should extend the base class');
			});

			t.it('Should extend specified class', function (t) {
				var OtherClass = ClassManager.define('OtherClass', {}),
					TestClass = ClassManager.define('TestClass', {
						extend: OtherClass
					}),
					instance = new TestClass();

				t.ok(instance instanceof OtherClass, 'Should extend specified class');
			});
		});
	});

	t.describe('Helper Functions', function (t) {

		t.describe('Merge', function (t) {
			t.it('Should merge properties from objects', function (t) {
				var target = { a: 1 },
					source = { b: 2 };

				ClassManager.merge(target, source);

				t.is(target.a, 1, 'Should retain target properties');
				t.is(target.b, 2, 'Should merge property from source object');
			});

			t.it('Should merge functions from object', function (t) {
				var target = { a: function () { return 'a'; } },
					source = { b: function () { return 'b'; } };

				ClassManager.merge(target, source);

				t.is(target.a(), 'a', 'Should retain target functions');
				t.is(target.b(), 'b', 'Should merge functions from source object');
			});

			t.it('Should not make changes to source object', function (t) {
				var target = { a: 1 },
					source = { b: 2 };

				ClassManager.merge(target, source);

				t.expect(source.a).not.toBeDefined();
				t.expect(source.b).toBe(2);
			});
		});
	});
});
